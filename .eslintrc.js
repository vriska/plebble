module.exports = {
    root: true,
    parser: '@typescript-eslint/parser',
    plugins: ['@typescript-eslint'],
    env: {
        'es2021': true,
        'shared-node-browser': true,
        'commonjs': true
    },
    globals: {
        // Pebble
        Pebble: 'readable',

        // WebSockets
        WebSocket: 'readable',
        CloseEvent: 'readable',
        MessageEvent: 'readable',

        // XMLHttpRequest
        XMLHttpRequest: 'readable',
        XMLHttpRequestEventTarget: 'readable',
        XMLHttpRequestUpload: 'readable',
        FormData: 'readable',

        // Geolocation
        navigator: 'readable',

        // LocalStorage
        Storage: 'readable',
        localStorage: 'readable',
        StorageEvent: 'readable',

        // whatwg-fetch
        fetch: 'readable',
        Headers: 'readable',
        Request: 'readable',
        Response: 'readable'
    },
    extends: [
        'eslint:recommended',
        'plugin:@typescript-eslint/recommended'
    ],
    parserOptions: {
        ecmaVersion: 12,
        sourceType: 'module'
    },
    rules: {
        'indent': [
            'error',
            4
        ],
        'linebreak-style': [
            'error',
            'unix'
        ],
        'quotes': [
            'error',
            'single'
        ],
        'semi': [
            'error',
            'always'
        ],
        'no-unused-vars': 'off',
        '@typescript-eslint/no-explicit-any': 'off',
    },
    overrides: [
        {
            files: ['webpack.config.js', '.eslintrc.js'],
            env: {
                node: true
            }
        }
    ]
};
