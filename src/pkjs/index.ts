function callPromise<A extends any[], R, E>(func: (...args: [...A, (res: R) => void, (err: E) => void]) => void, ...args: A): PromiseLike<R> {
    return new Promise((resolve, reject) => func(...args, resolve, reject));
}

console.log('Started!');

Pebble.addEventListener('ready', () => {
    console.log('Ready!');

    // TODO: make offset configurable
    const socket = new WebSocket('wss://api.sibr.dev/corsmechanics/blaseball/events/streamSocket?before_offset=2848034');

    socket.onmessage = async (event) => {
        try {
            console.log('Received message...');

            const data = JSON.parse(event.data);

            const crabsId = '8d87c468-699a-47a8-b40d-cfb73a5660ad';

            const crabsGame = data.value.games.schedule.find((x: {awayTeam: string, homeTeam: string}) => {
                return x.awayTeam === crabsId || x.homeTeam == crabsId; // TODO: don't hardcode
            });

            const message = {
                'LastUpdate': crabsGame.lastUpdate
            };

            console.log('Sending update...');
            await callPromise(Pebble.sendAppMessage.bind(Pebble), message);
            console.log('Sent!');
        } catch (err) {
            console.error('Exception in promise: ', err);
        }
    };
});
