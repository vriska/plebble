#include <stdbool.h>
#include <pebble.h>

extern uintptr_t strlen(const char* str);

static Window *s_window;
static TextLayer *s_text_layer;
static AppSync s_sync;
static uint8_t s_sync_buffer[1024];

static void state_update(const uint32_t key, const Tuple* new, const Tuple* old, void* ctx) {
    APP_LOG(APP_LOG_LEVEL_DEBUG, "State update");

    if (key == MESSAGE_KEY_LastUpdate) {
        text_layer_set_text(s_text_layer, new->value->cstring);
    }
}

static void sync_error(DictionaryResult dict_error, AppMessageResult app_message_error, void* ctx) {
    APP_LOG(APP_LOG_LEVEL_ERROR, "App Message Sync Error: %d", app_message_error);
}

static void prv_window_load(Window *window) {
    Layer *window_layer = window_get_root_layer(window);
    GRect bounds = layer_get_bounds(window_layer);

    s_text_layer = text_layer_create(GRect(0, 0, bounds.size.w, bounds.size.h));
    text_layer_set_text(s_text_layer, "Waiting for message...");
    text_layer_set_text_alignment(s_text_layer, GTextAlignmentCenter);
    layer_add_child(window_layer, text_layer_get_layer(s_text_layer));

    Tuplet initial_values[] = {
        TupletCString(MESSAGE_KEY_LastUpdate, "Waiting for message..."),
    };
    app_sync_init(&s_sync, s_sync_buffer, sizeof(s_sync_buffer), initial_values, ARRAY_LENGTH(initial_values), state_update, sync_error, 0);
}

static void prv_window_unload(Window *window) {
    text_layer_destroy(s_text_layer);
}

static void prv_init(void) {
    s_window = window_create();
    window_set_window_handlers(s_window, (WindowHandlers) {
        .load = prv_window_load,
        .unload = prv_window_unload,
    });
    const bool animated = true;
    window_stack_push(s_window, animated);

    app_message_open(1024, 64);
}

static void prv_deinit(void) {
    window_destroy(s_window);
    app_sync_deinit(&s_sync);
}

int main(void) {
    prv_init();

    APP_LOG(APP_LOG_LEVEL_DEBUG, "Done initializing, pushed window: %p", s_window);

    app_event_loop();
    prv_deinit();
}
