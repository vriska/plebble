{
    inputs.pebble.url = "github:Sorixelle/pebble.nix";
    inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-21.05";
    inputs.flake-utils.url = "github:numtide/flake-utils";

    outputs = { self, pebble, nixpkgs, flake-utils }:
        flake-utils.lib.eachSystem [ "i686-linux" "x86_64-linux" "x86_64-darwin" ] (system: with nixpkgs.legacyPackages.${system}; rec {
            devShell = pebble.pebbleEnv.${system} { };

            packages.yarnDeps = mkYarnModules rec {
                name = "plebble-yarn-deps";
                pname = name;
                version = "git";

                packageJSON = ./package.json;
                yarnLock = ./yarn.lock;
            };

            defaultPackage = pebble.buildPebbleApp.${system} {
                name = "Plebble";
                src = ./.;
                type = "watchapp";

                description = "Blaseball client for Pebble";
                category = "Games";
                releaseNotes = builtins.readFile ./NEWS.md;

                banner = "assets/banner.png";
                smallIcon = "assets/smallIcon.png";
                largeIcon = "assets/largeIcon.png";
                screenshots.basalt = [ "assets/basalt.png" ];

                homepage = "https://gitlab.com/vriska/plebble";
                sourceUrl = "https://gitlab.com/vriska/plebble";

                buildPhase = ''
                cp -r ${packages.yarnDeps}/node_modules .
                chmod -R a+rwX node_modules/
                rm -rf node_modules/plebble
                export PATH=${writeShellScriptBin "npm" ''
                if [[ "$1" != install ]]; then
                    exec ${pebble.inputs.nixpkgs.legacyPackages.${system}.nodejs}/bin/npm "$@"
                fi
                ''}/bin:$PATH
                pebble build
                '';
            };
        });
}
