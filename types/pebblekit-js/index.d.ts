// Type definitions for PebbleKit JS
// Project (mirror): https://developer.rebble.io/developer.pebble.com/docs/pebblekit-js/index.html
// Definitions by: leo60228 <https://vriska.dev>

declare namespace Pebble {
    // events
    interface Event {
        type: string;
    }

    interface ReadyEvent extends Event {
        type: 'ready';
    }

    interface Dictionary {
        [key: string]: number | string | number[];
    }

    interface AppMessageEvent extends Event {
        type: 'appmessage';
        payload: Dictionary;
    }

    interface ShowConfigurationEvent extends Event {
        type: 'showConfiguration';
    }

    interface WebviewClosedEvent extends Event {
        type: 'webviewclosed';
        response?: string;
    }

    // types defined in documentation
    type TimelineTopicsCallback = (list: string[]) => void;
    type AppGlanceReloadFailureCallback = (appGlanceSlices: AppGlanceSlice[]) => void;
    type AppMessageAckCallback = (data: { transactionId: number }) => void;
    type AppMessageNackCallback = (data: { transactionId: number }, error: string) => void;
    type TimelineTokenCallback = (token: string) => void;
    type AppGlanceReloadSuccessCallback = (appGlanceSlices: AppGlanceSlice[]) => void;

    interface WatchInfo {
        platform: string;
        model: string;
        language: string;
        firmware: {
            major: number;
            minor: number;
            patch: number;
            suffix: string;
        }
    }

    interface AppGlanceSlice {
        expirationTime?: string;
        layout: {
            icon: string;
            subtitleTemplateString: string;
        }
    }

    // functions
    interface PebbleEventMap {
        'ready': ReadyEvent;
        'appmessage': AppMessageEvent;
        'showConfiguration': ShowConfigurationEvent;
        'webviewclosed': WebviewClosedEvent;
    }

    function addEventListener<K extends keyof PebbleEventMap>(type: K, callback: (event: PebbleEventMap[K]) => void): void;
    function addEventListener(type: string, callback: (event: Event) => void): void;

    function removeEventListener<K extends keyof PebbleEventMap>(type: K, callback: (event: PebbleEventMap[K]) => void): void;
    function removeEventListener(type: string, callback: (event: Event) => void): void;

    function showSimpleNotificationOnPebble(title: string, body: string): void;

    function sendAppMessage(data: Dictionary, onSuccess: AppMessageAckCallback, onFailure: AppMessageNackCallback): any;

    function getTimelineToken(onSuccess: TimelineTokenCallback, onFailure: () => void): void;

    function timelineSubscribe(topic: string, onSuccess: () => void, onFailure: () => void): void;

    function timelineUnsubscribe(topic: string, onSuccess: () => void, onFailure: () => void): void;

    function timelineSubscriptions(onSuccess: TimelineTopicsCallback, onFailure: () => void): void;

    function getActiveWatchInfo(): WatchInfo;

    function getAccountToken(): string;

    function getWatchToken(): string;

    function appGlanceReload(appGlanceSlices: AppGlanceSlice[], onSuccess: AppGlanceReloadSuccessCallback, onFailure: AppGlanceReloadFailureCallback): void;

    function openURL(url: string): void;
}
