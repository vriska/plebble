const path = require('path');
const os = require('os');
const webpack = require('webpack');

function sdkPath(p) {
    const baseSdkPath = os.type() === 'Darwin' ? 'Library/Application Support/Pebble SDK/' : '.pebble-sdk/';
    return path.relative(__dirname, path.resolve(os.homedir(), baseSdkPath, 'SDKs/current/sdk-core', p));
}

module.exports = {
    mode: 'production',
    devtool: 'source-map',
    entry: [
        sdkPath('pebble/common/include/_pkjs_shared_additions.js'),
        './src/pkjs/index.ts'
    ],
    output: {
        filename: 'pebble-js-app.js',
        path: path.resolve(__dirname, './build'),
        sourceMapFilename: 'pebble-js-app.js.map',
        chunkFormat: 'array-push',
        devtoolModuleFilenameTemplate: '[resource-path]',
        devtoolFallbackModuleFilenameTemplate: '[resource-path]?[hash]',
    },
    cache: {
        type: 'filesystem',
    },
    target: 'es5',
    resolve: {
        roots: [
            sdkPath('pebble/common/include')
        ],
        extensions: ['.js', '.json', '.ts'],
        alias: {
            'app_package.json': path.resolve(__dirname, './package.json'),
            'message_keys': path.resolve(__dirname, './build/js/message_keys.json')
        }
    },

    module: {
        rules: [
            {
                test: /\.ts$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [['@babel/preset-env', {
                            modules: false,
                            useBuiltIns: 'usage',
                            corejs: '3.16'
                        }], '@babel/preset-typescript']
                    },
                },
            },
            {
                test: /\.js$/,
                exclude: [
                    /node_modules[\\/]core-js/,
                    /node_modules[\\/]regenerator-runtime/,
                    /node_modules[\\/]webpack[\\/]buildin/,
                ],
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [['@babel/preset-env', {
                            modules: false,
                            useBuiltIns: 'usage',
                            corejs: '3.16'
                        }]]
                    },
                },
            }
        ]
    },

    plugins: [new webpack.ProvidePlugin({
        'fetch': 'whatwg-fetch'
    })]
};
